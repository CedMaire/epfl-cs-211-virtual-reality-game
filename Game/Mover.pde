class Mover {
  /*
  -----------------------------------------------------
   Some constants/variables needed for the mover.
   -----------------------------------------------------
   */
  PVector location;
  PVector velocity;
  PVector gravity;
  PVector friction;

  final float radius = 25 * scale;
  final float gravityConst = 9.81 / 7.0;

  final float normalForce = 1;
  final float mu = 0.9;
  final float frictionMagnitude = normalForce * mu;

  double velocityNorm;

  /*
  -----------------------------------------------------
   Constructor for the Mover class.
   -----------------------------------------------------
   */
  Mover() {
    location = new PVector(0, 0, 0);
    velocity = new PVector(0, 0, 0);
    gravity = new PVector(0, 0, 0);
  }

  /*
  -----------------------------------------------------
   Functions needed to update the mover object.
   -----------------------------------------------------
   */
  void display() {
    stroke(0);
    strokeWeight(2);
    translate(location.x, location.y, location.z);
    sphere(radius);
  }

  void update() {
    gravityUpdate();
    frictionUpdate();
    locationUpdate();

    velocityNorm = Math.sqrt(Math.pow(velocity.x, 2) + Math.pow(velocity.z, 2));
  }

  void locationUpdate() {
    location.add(velocity.add(gravity).add(friction));
  }

  void frictionUpdate() {
    friction = velocity.get();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
  }

  void gravityUpdate() {
    gravity.x = sin(thetaZ) * gravityConst;
    gravity.z = sin(thetaX) * gravityConst;
  }

  /*
  -----------------------------------------------------
   Functions needed to check the collisions (physic engine).
   -----------------------------------------------------
   */
  void checkEdges() {
    if (location.x + radius > boxSizeX / 2.0) {
      velocity.x *= -1;
      location.x = boxSizeX / 2.0 - radius;

      changeScore(-velocityNorm);
    } else if (location.x - radius < -boxSizeX / 2.0) {
      velocity.x *= -1;
      location.x = -boxSizeX / 2.0 + radius;

      changeScore(-velocityNorm);
    }
    if (location.z + radius > boxSizeZ / 2.0) {
      velocity.z *= -1;
      location.z = boxSizeZ / 2.0 - radius;

      changeScore(-velocityNorm);
    } else if (location.z - radius < -boxSizeZ / 2.0) {
      velocity.z *= -1;
      location.z = -boxSizeZ / 2.0 + radius;

      changeScore(-velocityNorm);
    }
  }

  void checkCylinderCollision() {
    for (PVector vect : closedCylinders) {
      PVector threeDVect = new PVector(-vect.x, 0, vect.y);

      if (PVector.dist(threeDVect, location) <= radius + cylinderBaseSize) {
        PVector normalVect = PVector.sub(location, threeDVect).normalize();
        float twoDotProd = 2 * PVector.dot(velocity, normalVect);

        velocity = PVector.sub(velocity, PVector.mult(normalVect, twoDotProd));

        PVector sphereLocNoTrespass = PVector.mult(normalVect, cylinderBaseSize + radius);
        location.x = threeDVect.x + sphereLocNoTrespass.x;
        location.z = threeDVect.z + sphereLocNoTrespass.z;

        changeScore(velocityNorm);
      }
    }
  }

  void changeScore(double scoreToAdd) {
    scorePoints += scoreToAdd;
    lastScorePoints = scoreToAdd;
  }
}