import java.util.TimerTask;
import java.util.Timer;
import processing.video.*;

/*
  -----------------------------------------------------
 Some constants/variables needed for the sketch.
 -----------------------------------------------------
 */
final float scale = 0.7;
final int widthPixel = 500;
final int heightPixel = 500;
final int cameraZ = 1000;

final float boxSizeX = 360 * scale;
final float boxSizeY = 15 * scale;
final float boxSizeZ = 360 * scale;

final float deltaDefault = .03;
float delta = deltaDefault;
final float deltasDelta = .005;

final float angleDefault = 0;
float thetaX = angleDefault;
float thetaZ = angleDefault;
final float maxAngle = PI / 3.0;
final float editAngle = PI / 2.0;

Mover mover;
final float sphereRadius = 25 * scale;

ArrayList<PVector> closedCylinders;
ArrayList<Double> totalScores;

final int SINGLECLICK = 1;
final int DOUBLECLICK = 2 * SINGLECLICK;

boolean PAUSE = false;

PGraphics scoreBoard;
PGraphics topView;
PGraphics score;
PGraphics barChart;

double scorePoints = 0;
double lastScorePoints = 0;

HScrollbar hScrollBar;
float scrollScale = 1;

Movie cam;
ImageProcessing imgProc;
PImage img;

/*
  -----------------------------------------------------
 Sketch
 -----------------------------------------------------
 */
void settings() {
  size(widthPixel, heightPixel, P3D);
}

void setup() {
  noStroke();

  cam = new Movie(this, "testvideo.avi"); //Put the video in the same directory
  cam.loop();

  if (cam.available() == true) {
    cam.read();
  }
  img = cam.get();

  imgProc = new ImageProcessing();
  String[] args = { "Image Processing Window" };
  PApplet.runSketch(args, imgProc);

  // We initialize the score-visualization bar.
  scoreBoard = createGraphics(width, (int) (height / 5.0), P2D);
  topView = createGraphics(scoreBoard.height - 20, scoreBoard.height - 20, P2D);
  score = createGraphics(topView.width + 10, topView.height + 10, P2D);
  barChart = createGraphics(width - topView.width - score.width - 60, scoreBoard.height - 30, P2D);

  mover = new Mover();

  hScrollBar = new HScrollbar(10 + 2 * scoreBoard.height, barChart.height + 4 * height / 5.0 + 15, barChart.width, 30);

  totalScores = new ArrayList<Double>();

  closedCylinders = new ArrayList<PVector>();
  drawCylinder();

  Timer timer = new Timer();
  timer.schedule(new timedExec(), 0, 2500);
}

void draw() {
  if (!PAUSE) {
    setEnv();

    fill(0, 255, 0);

    if (cam.available() == true) {
      cam.read();
    }
    img = cam.get();

    PVector rot = imgProc.getThreeDRots();
    thetaX = rot.x;
    thetaZ = rot.y;

    createBox(-thetaX, thetaZ);
    updateMover(-thetaX, thetaZ);

    fill(120, 230, 70);
    drawScoreBoard();

    // We draw the score-visualization bar.
    image(scoreBoard, 0, 4 * height / 5.0);
    drawTopView();
    image(topView, 10, 4 * height / 5.0 + 10);
    fill(0, 0, 0);
    drawScore();
    image(score, 10 + (int) (height / 5.0) - 10, 4 * height / 5.0 + 5);
    fill(255, 200, 255);
    drawBarChart();
    image(barChart, 10 + (int) (height / 5.0) + (height / 5.0), 4 * height / 5.0 + 5);

    hScrollBar.update();
    hScrollBar.display();
    scrollScale = hScrollBar.getPos();
  } else {
    clear();
    setEnv();
    enterEditMode(-editAngle, 0);
  }
}

/*
  -----------------------------------------------------
 Creation
 -----------------------------------------------------
 */
void setEnv() {
  directionalLight(50, 100, 125, 0, 1, 0);
  ambientLight(100, 150, 100);
  background(200);
}

void createBox(float thetaX, float thetaZ) {
  pushMatrix();

  translate(width/2.0, height/2.0, 0);
  rotateEnv(thetaX, thetaZ);
  box(boxSizeX, boxSizeY, boxSizeZ);
  updateCylinder();

  popMatrix();
}

void updateMover(float thetaX, float thetaZ) {
  pushMatrix();

  updateEnv(thetaX, thetaZ);

  mover.checkEdges();
  mover.checkCylinderCollision();
  mover.update();
  mover.display();

  popMatrix();
}

void rotateEnv(float thetaX, float thetaZ) {
  rotateX(thetaX);
  rotateZ(thetaZ);
}

void enterEditMode(float thetaX, float thetaZ) {
  createBox(thetaX, thetaZ);

  pushMatrix();

  updateEnv(thetaX, thetaZ);
  mover.display();

  popMatrix();
}

void updateEnv(float thetaX, float thetaZ) {
  translate(width/2.0, height/2.0, 0);

  rotateEnv(thetaX, thetaZ);

  translate(0, -boxSizeY / 2.0 - mover.radius, 0);
}

void drawScoreBoard() {
  scoreBoard.beginDraw();
  scoreBoard.background(250);
  scoreBoard.endDraw();
}

void drawTopView() {
  topView.beginDraw();
  topView.background(150);

  PVector ball = new PVector(map(mover.location.x, -boxSizeX / 2.0, boxSizeX / 2.0, 0, topView.width), 
    map(mover.location.z, -boxSizeZ / 2.0, boxSizeZ / 2.0, 0, topView.height));
  topView.ellipse(ball.x, ball.y, 7, 7);

  for (PVector vect : closedCylinders) {
    PVector mappedVect = new PVector(map(vect.x, boxSizeX / 2.0, -boxSizeX / 2.0, 0, topView.width), 
      map(vect.y, -boxSizeZ / 2.0, boxSizeZ / 2.0, 0, topView.height));
    topView.ellipse(mappedVect.x, mappedVect.y, 12, 12);
  }

  topView.endDraw();
}

void drawScore() {
  score.beginDraw();

  score.background(60);
  fill(0, 255, 0);
  score.text("Total Score:", 10, 10);
  score.text((float) scorePoints, 10, 20);
  score.text("Velocity:", 10, 40);
  score.text((float) mover.velocityNorm, 10, 50);
  score.text("Last Score:", 10, 70);
  score.text((float) lastScorePoints, 10, 80);

  score.endDraw();
}

void drawBarChart() {
  barChart.beginDraw();

  barChart.background(120);
  float widthSquare = 0;
  ArrayList<Double> temp = new ArrayList(totalScores);

  for (double scorePoint : temp) {
    int numberOfSquares = (int) Math.abs(scorePoint) / 50;
    float heightSquare = barChart.height - 10 * scrollScale;

    for (int i = 0; i < numberOfSquares; i++) {
      if (scorePoint < 0) {
        barChart.fill(255, 0, 0);
      } else {
        barChart.fill(0, 255, 0);
      }

      barChart.stroke(0, 0, 0);
      barChart.rect(widthSquare, heightSquare, 10 * scrollScale, 10 * scrollScale, 7);
      heightSquare -= 10 * scrollScale;
    }

    widthSquare += 10 * scrollScale;
  }

  barChart.endDraw();
}

/*
  -----------------------------------------------------
 Interaction
 -----------------------------------------------------
 */
void mouseWheel(MouseEvent event) {
  float e = event.getCount();

  if (e < 0) {
    if (delta < 20 * deltasDelta) {
      delta += deltasDelta;
    }
  } else if (e > 0) {
    if (delta > 2 * deltasDelta) {
      delta -= deltasDelta;
    }
  }
}

void mouseDragged() {
  if (!PAUSE) {
    if (mouseY < 4 * height / 5.0) {
      if (mouseX - pmouseX < 0) {
        if (thetaZ > -maxAngle) {
          thetaZ -= delta;
        }
      } else if (mouseX - pmouseX > 0) {
        if (thetaZ < maxAngle) {
          thetaZ += delta;
        }
      }

      if (mouseY - pmouseY < 0) {
        if (thetaX > -maxAngle) {
          thetaX -= delta;
        }
      } else if (mouseY - pmouseY > 0) {
        if (thetaX < maxAngle) {
          thetaX += delta;
        }
      }
    }
  }
}

void mouseClicked(MouseEvent event) {
  if (!PAUSE && event.getCount() == DOUBLECLICK) {
    delta = deltaDefault;
    thetaX = angleDefault;
    thetaZ = angleDefault;
  }

  if (PAUSE && event.getCount() == SINGLECLICK) {
    boolean cylinderAlreadyHere = false;
    PVector mouseVectForBall = new PVector(mouseX - width/2, mouseY - height/2);
    PVector mouseVectForCyl = new PVector((width / 2.0 - mouseX), -(height / 2.0 - mouseY));
    PVector location2D = new PVector(mover.location.x, mover.location.z);

    for (PVector vect : closedCylinders) {
      if (PVector.dist(mouseVectForCyl, vect) < 2 * cylinderBaseSize) {
        cylinderAlreadyHere = true;
      }
    }

    if (PVector.dist(mouseVectForBall, location2D) > cylinderBaseSize + mover.radius && !cylinderAlreadyHere) {
      if (mouseX < boxSizeX - cylinderBaseSize + widthPixel / 2.0 - boxSizeX / 2.0 && mouseX > cylinderBaseSize + widthPixel / 2.0 - boxSizeX / 2.0) {
        if (mouseY < boxSizeZ - cylinderBaseSize + heightPixel / 2.0 - boxSizeZ / 2.0 && mouseY > cylinderBaseSize + heightPixel / 2.0 - boxSizeZ / 2.0) {
          pushMatrix();

          rotateEnv(PI / 2.0, 0);
          closedCylinders.add(new PVector((width / 2.0 - mouseX), -(height / 2.0 - mouseY)));

          popMatrix();
        }
      }
    }
  }
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == SHIFT) {
      PAUSE = true;
    }
  }
}

void keyReleased() {
  if (key == CODED) {
    if (keyCode == SHIFT) {
      PAUSE = false;
    }
  }
}

/*
  -----------------------------------------------------
 TimerTask
 -----------------------------------------------------
 */
class timedExec extends TimerTask {
  public void run() {
    totalScores.add(scorePoints);
  }
}