/*
  -----------------------------------------------------
 Some constants/variables needed for the cylinders.
 -----------------------------------------------------
 */
final float cylinderBaseSize = 15 * scale;
final float cylinderHeight = 60 * scale;
final int cylinderResolution = 40;

PShape closedCylinder = new PShape();

/*
  -----------------------------------------------------
 Some functions needed to draw the cylinders. Code is partially taken from the PDF of the course.
 -----------------------------------------------------
 */
void drawCylinder() {
  float angle;
  float[] x = new float[cylinderResolution + 1];
  float[] y = new float[cylinderResolution + 1];

  for (int i = 0; i < x.length; i++) {
    angle = (TWO_PI / cylinderResolution) * i;

    x[i] = sin(angle) * cylinderBaseSize;
    y[i] = cos(angle) * cylinderBaseSize;
  }

  closedCylinder = createShape();

  // Draw the bottom of the cylinder
  closedCylinder.beginShape(TRIANGLES);
  for (int i = 0; i < x.length; i++) {
    closedCylinder.vertex(x[i], 0, y[i]);
    closedCylinder.vertex(0, 0, 0);
  }
  closedCylinder.endShape(CLOSE);

  // Draw the top of the cylinder
  closedCylinder.beginShape(TRIANGLES);
  for (int i = 0; i < x.length; i++) {
    closedCylinder.vertex(x[i], cylinderHeight, y[i]);
    closedCylinder.vertex(0, cylinderHeight, 0);
  }
  closedCylinder.endShape(CLOSE);

  // Draw the border of the cylinder
  closedCylinder.beginShape(QUAD_STRIP);
  for (int i = 0; i < x.length; i++) {
    closedCylinder.vertex(x[i], 0, y[i]);
    closedCylinder.vertex(x[i], cylinderHeight, y[i]);
  }
  closedCylinder.endShape(CLOSE);
}

void updateCylinder() {
  for (PVector vect : closedCylinders) {
    pushMatrix();
    translate(-vect.x, -cylinderHeight, vect.y);
    shape(closedCylinder);

    popMatrix();
  }
}