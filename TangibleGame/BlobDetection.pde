import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

class BlobDetection {

  PImage findConnectedComponents(PImage input, boolean onlyBiggest) {
    input.loadPixels();

    // First pass: label the pixels and store labels’ equivalences
    int[] labels = new int[input.width * input.height];
    List<TreeSet<Integer>> labelsEquivalences = new ArrayList<TreeSet<Integer>>();

    int currentLabel = 1;

    // First row
    for (int x = 0; x < input.width; ++x) {
      int y = 0;

      if (isWhite(x, y, input)) { // White
        if (x == 0) { // First pixel
          labels[y * input.width + x] = currentLabel;

          TreeSet<Integer> temp = new TreeSet();
          temp.add(currentLabel);
          labelsEquivalences.add(temp);

          currentLabel += 1;
        } else { // Rest pixel
          int labelPx4 = labels[y * input.width + (x - 1)];

          if (labelPx4 == Integer.MAX_VALUE) { // Left neighbor is black
            labels[y * input.width + x] = currentLabel;

            TreeSet<Integer> temp = new TreeSet();
            temp.add(currentLabel);
            labelsEquivalences.add(temp);

            currentLabel += 1;
          } else { // Left neighbor is white
            labels[y * input.width + x] = labelPx4;
          }
        }
      } else { // Black
        labels[y * input.width + x] = Integer.MAX_VALUE;
      }
    }

    // Rest img
    for (int y = 1; y < input.height; ++y) {
      for (int x = 0; x < input.width; ++x) {
        if (isWhite(x, y, input)) { // White
          if (x == 0) { // First columns
            int labelPx2 = labels[(y - 1) * input.width + x];
            int labelPx3 = labels[(y - 1) * input.width + (x + 1)];

            if (labelPx2 == labelPx3) { // Two neighbors are equal
              if (labelPx2 == Integer.MAX_VALUE) { // They are black
                labels[y * input.width + x] = currentLabel;

                TreeSet<Integer> temp = new TreeSet();
                temp.add(currentLabel);
                labelsEquivalences.add(temp);

                currentLabel += 1;
              } else { // They are white
                labels[y * input.width + x] = labelPx2;
              }
            } else { // Two neighbors are NOT equal
              int min = Math.min(labelPx2, labelPx3);

              labels[y * input.width + x] = min;

              TreeSet<Integer> temp = new TreeSet();
              treeSetAddAll(labelPx2, labelsEquivalences, temp);
              treeSetAddAll(labelPx3, labelsEquivalences, temp);
              labEqSet(labelPx2, labelsEquivalences, temp);
              labEqSet(labelPx3, labelsEquivalences, temp);
            }
          } else if (x == input.width - 1) { // Last columns
            int labelPx1 = labels[(y - 1) * input.width + (x - 1)];
            int labelPx2 = labels[(y - 1) * input.width + x];
            int labelPx4 = labels[y * input.width + (x - 1)];

            if (labelPx2 == labelPx1 && labelPx2 == labelPx4) { // Three neighbors are equal
              if (labelPx2 == Integer.MAX_VALUE) { // They are black
                labels[y * input.width + x] = currentLabel;

                TreeSet<Integer> temp = new TreeSet();
                temp.add(currentLabel);
                labelsEquivalences.add(temp);

                currentLabel += 1;
              } else { // They are white
                labels[y * input.width + x] = labelPx2;
              }
            } else { // Two neighbors are NOT equal
              int min = Math.min(labelPx2, Math.min(labelPx1, labelPx4));

              labels[y * input.width + x] = min;

              TreeSet<Integer> temp = new TreeSet();
              treeSetAddAll(labelPx1, labelsEquivalences, temp);
              treeSetAddAll(labelPx2, labelsEquivalences, temp);
              treeSetAddAll(labelPx4, labelsEquivalences, temp);
              labEqSet(labelPx1, labelsEquivalences, temp);
              labEqSet(labelPx2, labelsEquivalences, temp);
              labEqSet(labelPx4, labelsEquivalences, temp);
            }
          } else { // Neither in first column, nor first row
            int labelPx1 = labels[(y - 1) * input.width + (x - 1)];
            int labelPx2 = labels[(y - 1) * input.width + x];
            int labelPx3 = labels[(y - 1) * input.width + (x + 1)];
            int labelPx4 = labels[y * input.width + (x - 1)];

            if (labelPx1 == labelPx2 && labelPx1 == labelPx3 && labelPx1 == labelPx4) { // All neighbors are equal
              if (labelPx1 == Integer.MAX_VALUE) { // All black
                labels[y * input.width + x] = currentLabel;

                TreeSet<Integer> temp = new TreeSet();
                temp.add(currentLabel);
                labelsEquivalences.add(temp);

                currentLabel += 1;
              } else { // All white
                labels[y * input.width + x] = labelPx1;
              }
            } else { // Neighbors are not all equal
              int min = Math.min(Math.min(labelPx1, labelPx2), Math.min(labelPx3, labelPx4));

              labels[y * input.width + x] = min;

              TreeSet<Integer> temp = new TreeSet();
              treeSetAddAll(labelPx1, labelsEquivalences, temp);
              treeSetAddAll(labelPx2, labelsEquivalences, temp);
              treeSetAddAll(labelPx3, labelsEquivalences, temp);
              treeSetAddAll(labelPx4, labelsEquivalences, temp);
              labEqSet(labelPx1, labelsEquivalences, temp);
              labEqSet(labelPx2, labelsEquivalences, temp);
              labEqSet(labelPx3, labelsEquivalences, temp);
              labEqSet(labelPx4, labelsEquivalences, temp);
            }
          }
        } else { // Pixel is black
          labels[y * input.width + x] = Integer.MAX_VALUE;
        }
      }
    }

    // Second pass: re-label the pixels by their equivalent class
    // if onlyBiggest == true, count the number of pixels for each label
    int[] sizeOfLabels = new int[currentLabel];
    for (int i = 0; i < currentLabel; ++i) {
      sizeOfLabels[i] = 0;
    }

    int max = 0;
    int iOfMax = 0;

    for (int i = 0; i < input.width * input.height; ++i) {
      int label = labels[i];

      if (label != Integer.MAX_VALUE) {
        for (int j = 0; j < labelsEquivalences.size(); ++j) {
          if (labelsEquivalences.get(j).contains(label)) {
            labels[i] = labelsEquivalences.get(j).first();
            break;
          }
        }

        sizeOfLabels[labels[i]] += 1;

        if (onlyBiggest && sizeOfLabels[labels[i]] > max) {
          max = sizeOfLabels[labels[i]];
          iOfMax = labels[i];
        }
      }
    }

    // Finally,
    // if onlyBiggest == false, output an image with each blob colored in one uniform color
    // if onlyBiggest == true, output an image with the biggest blob colored in white and the others in black
    PImage result = createImage(input.width, input.height, RGB);

    for (int i = 0; i < input.width * input.height; ++i) {
      if (labels[i] == Integer.MAX_VALUE) {
        result.pixels[i] = color(0, 0, 0);
      } else {
        if (onlyBiggest) {
          if (labels[i] == iOfMax) {
            result.pixels[i] = color(255, 255, 255);
          } else {
            result.pixels[i] = color(0, 0, 0);
          }
        } else {
          int tempColor = 255 * labels[i] / currentLabel;
          result.pixels[i] = color(tempColor * ((labels[i] + 1) % 3), tempColor * ((labels[i] + 2) % 3), tempColor * (labels[i] % 3));
        }
      }
    }

    result.updatePixels();

    return result;
  }

  void treeSetAddAll(int labelPxi, List<TreeSet<Integer>> labelsEquivalences, TreeSet<Integer> treeSet) {
    if (labelPxi != Integer.MAX_VALUE) {
      treeSet.addAll(labelsEquivalences.get(labelPxi - 1));
    }
  }

  void labEqSet(int labelPxi, List<TreeSet<Integer>> labelsEquivalences, TreeSet<Integer> treeSet) {
    if (labelPxi != Integer.MAX_VALUE) {
      labelsEquivalences.set(labelPxi - 1, treeSet);
    }
  }

  boolean isWhite(int x, int y, PImage input) {
    return input.pixels[y * input.width + x] == color(255, 255, 255);
  }
}