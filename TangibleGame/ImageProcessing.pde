import java.util.Collections; //<>// //<>//
import gab.opencv.*;

class ImageProcessing extends PApplet {
  OpenCV opencv;

  BlobDetection blobber;
  QuadGraph quad;

  List<PVector> oldPoints = new ArrayList<PVector>();
  PVector threeDRots = new PVector(0, 0, 0);

  // Pre-compute the sin and cos values
  // Dimensions of the accumulator
  final float discretizationStepsPhi = 0.06f;
  final float discretizationStepsR = 2.5f;
  final int phiDim = (int) (Math.PI / discretizationStepsPhi + 1);
  float[] tabSin = new float[phiDim];
  float[] tabCos = new float[phiDim];
  float ang = 0;
  final float inverseR = 1.f / discretizationStepsR;

  void settings() {
    size(3 * 800, 600);

    for (int accPhi = 0; accPhi < phiDim; ang += discretizationStepsPhi, ++accPhi) {
      tabSin[accPhi] = (float) (Math.sin(ang) * inverseR);
      tabCos[accPhi] = (float) (Math.cos(ang) * inverseR);
    }
  }

  void setup() {
    opencv = new OpenCV(this, 100, 100);
    blobber = new BlobDetection();
    quad = new QuadGraph();

    for (int i = 0; i < 4; ++i) {
      oldPoints.add(new PVector(0, 0));
    }
  }

  void draw() {
    background(color(0, 0, 0));

    PImage result = thresholdHSB(img, 70, 140, 60, 255, 20, 165);
    PImage blob =  blobber.findConnectedComponents(result, true);

    result = convolute(blob);
    result = scharr(result);
    result = threshold(result, 160);

    image(img, 0 * 800, 0);

    List<PVector> lines = hough(result, 8);

    image(blob, 1 * 800, 0);
    image(result, 2 * 800, 0);

    List<PVector> points = quad.findBestQuad(lines, 800, 600, 360000, 9000, false);
    if (points.size() < 4) {
      points = new ArrayList<PVector>(oldPoints);
    } else {
      oldPoints = new ArrayList<PVector>(points);
    }

    for (int i = 0; i < 4; ++i) {
      ellipse(points.get(i).x, points.get(i).y, 16, 16);
    }

    // 2D to 3D
    TwoDThreeD twoDthreeD = new TwoDThreeD(img.width, img.height, 30);
    List<PVector> homoVects = new ArrayList<PVector>();
    for (PVector vect : points) {
      PVector temp = new PVector();
      temp.x = vect.x;
      temp.y = vect.y;
      temp.z = 1;

      homoVects.add(temp);
    }

    threeDRots = twoDthreeD.get3DRotations(homoVects);
  }

  PVector getThreeDRots() {
    return threeDRots;
  }

  PImage threshold(PImage img, int threshold) {
    // Create a new, initially transparent, ’result’ image
    PImage result = createImage(img.width, img.height, RGB);

    img.loadPixels();
    result.loadPixels();

    for (int i = 0; i < img.width * img.height; ++i) {
      if (brightness(img.pixels[i]) <= threshold) {
        result.pixels[i] = color(0, 0, 0);
      } else {
        result.pixels[i] = color(255, 255, 255);
      }
    }

    result.updatePixels();

    return result;
  }

  PImage thresholdHSB(PImage img, int minH, int maxH, int minS, int maxS, int minB, int maxB) {
    PImage result = createImage(img.width, img.height, HSB);

    img.loadPixels();
    result.loadPixels();

    for (int i = 0; i < img.width * img.height; ++i) {
      float hue = hue(img.pixels[i]);
      float saturation = saturation(img.pixels[i]);
      float bright = brightness(img.pixels[i]);

      if (minH <= hue && hue <= maxH && minS <= saturation && saturation <= maxS && minB <= bright && bright <= maxB) {
        result.pixels[i] = color(255, 255, 255);
      } else {
        result.pixels[i] = color(0, 0, 0);
      }
    }

    result.updatePixels();

    return result;
  }

  boolean imagesEqual(PImage img1, PImage img2) {
    if (img1.width != img2.width || img1.height != img2.height)
      return false;

    for (int i = 0; i < img1.width * img1.height; ++i)
      // Assuming that all the three channels have the same value
      if (red(img1.pixels[i]) != red(img2.pixels[i])) {
        return false;
      }

    return true;
  }

  PImage convolute(PImage img) {
    float[][] gaussianKernel = {
      { 9, 12, 9 }, 
      { 12, 15, 12 }, 
      { 9, 12, 9 }
    };

    float[][] kernel = gaussianKernel;

    float normFactor = 99.f;

    // Create a greyscale image (type: ALPHA) for output
    PImage result = createImage(img.width, img.height, ALPHA);

    // Clear the image
    for (int i = 0; i < img.width * img.height; ++i) {
      result.pixels[i] = color(0);
    }

    img.loadPixels();
    result.loadPixels();

    // kernel size N = 3
    //
    // for each (x,y) pixel in the image:
    //     - multiply intensities for pixels in the range
    //       (x - N/2, y - N/2) to (x + N/2, y + N/2) by the
    //       corresponding weights in the kernel matrix
    //     - sum all these intensities and divide it by normFactor
    //     - set result.pixels[y * img.width + x] to this value

    for (int y = 1; y < img.height - 1; ++y) {
      for (int x = 1; x < img.width - 1; ++x) {
        float sum = 0;
        int u = 0;

        for (int a = y - (kernel.length / 2); a <= y + (kernel.length / 2); ++a) {
          int v = 0;

          for (int b = x - (kernel[0].length / 2); b <= x + (kernel[0].length / 2); ++b) {
            sum += kernel[u][v] * brightness(img.pixels[Math.max(0, Math.min(img.height - 1, a)) * img.width + Math.max(0, Math.min(img.width - 1, b))]);

            v += 1;
          }
          u += 1;
        }

        result.pixels[y * result.width + x] = color((sum / normFactor));
      }
    }

    result.updatePixels();

    return result;
  }

  PImage scharr(PImage img) {
    float[][] vKernel = {
      { 3, 0, -3 }, 
      { 10, 0, -10 }, 
      { 3, 0, -3 }
    };

    float[][] hKernel = {
      { 3, 10, 3}, 
      { 0, 0, 0}, 
      { -3, -10, -3 }
    };

    int kernelLength = vKernel.length;

    PImage result = createImage(img.width, img.height, ALPHA);

    // Clear the image
    for (int i = 0; i < img.width * img.height; i++) {
      result.pixels[i] = color(0);
    }

    float max = 0;
    float[] buffer = new float[img.width * img.height];

    // *************************************
    // Implement here the double convolution
    // *************************************  
    for (int y = 0; y < img.height; ++y) {
      for (int x = 0; x < img.width; ++x) {
        float sum_h = 0;
        float sum_v = 0;
        int u = 0;

        for (int a = y - (kernelLength / 2); a <= y + (kernelLength / 2); ++a) {
          int v = 0;

          for (int b = x - (kernelLength / 2); b <= x + (kernelLength / 2); ++b) {
            sum_h += hKernel[u][v] * brightness(img.pixels[Math.max(0, Math.min(img.height - 1, a)) * img.width + Math.max(0, Math.min(img.width - 1, b))]);
            sum_v += vKernel[u][v] * brightness(img.pixels[Math.max(0, Math.min(img.height - 1, a)) * img.width + Math.max(0, Math.min(img.width - 1, b))]);

            v += 1;
          }
          u += 1;
        }

        float sum = sqrt(pow(sum_h, 2) + pow(sum_v, 2));
        if (sum > max) {
          max = sum;
        }

        buffer[y * img.width + x] = sum;
      }
    }

    for (int y = 2; y < img.height - 2; ++y) { // Skip top and bottom edges
      for (int x = 2; x < img.width - 2; ++x) { // Skip left and right
        int val = (int) ((buffer[y * img.width + x] / max) * 255);
        result.pixels[y * img.width + x] = color(val);
      }
    }

    result.updatePixels();

    return result;
  }

  List<PVector> hough(PImage edgeImg, int nLines) {
    int minVotes = 80;
    ArrayList<Integer> bestCandidates = new ArrayList<Integer>();

    // The max radius is the image diagonal, but it can be also negative
    int rDim = (int) ((sqrt(edgeImg.width*edgeImg.width + edgeImg.height*edgeImg.height) * 2) / discretizationStepsR + 1);

    // Our accumulator
    int[] accumulator = new int[phiDim * rDim];

    // Fill the accumulator: on edge points (ie, white pixels of the edge
    // image), store all possible (r, phi) pairs describing lines going
    // through the point.
    for (int y = 0; y < edgeImg.height; ++y) {
      for (int x = 0; x < edgeImg.width; ++x) {
        // Are we on an edge?
        if (brightness(edgeImg.pixels[y * edgeImg.width + x]) != 0) {

          // ...determine here all the lines (r, phi) passing through
          // pixel (x,y), convert (r,phi) to coordinates in the
          // accumulator, and increment accordingly the accumulator.
          // Be careful: r may be negative, so you may want to center onto
          // the accumulator: r += rDim / 2

          for (int i = 0; i < phiDim; i += 1) {
            float r = Math.round(x * tabCos[i] + y * tabSin[i]);
            accumulator[(int) ((r + rDim / 2) + i * rDim)] += 1;
          }
        }
      }
    }

    int neighbourhood = 10;
    for (int i = 0; i < accumulator.length; ++i) {
      if (accumulator[i] > minVotes) {
        boolean isMax = true;

        for (int j = Math.max(0, i - neighbourhood / 2); j < Math.min(accumulator.length, i + neighbourhood / 2); ++j) {
          if (accumulator[j] > accumulator[i]) {
            isMax = false;
          }
        }

        if (isMax) {
          bestCandidates.add(i);
        }
      }
    }

    Collections.sort(bestCandidates, new HoughComparator(accumulator));
    bestCandidates = new ArrayList<Integer>(bestCandidates.subList(0, Math.min(bestCandidates.size(), nLines)));

    ArrayList<PVector> lines = new ArrayList<PVector>();

    for (int idx = 0; idx < accumulator.length; ++idx) {
      if (bestCandidates.contains(idx)) {
        // First, compute back the (r, phi) polar coordinates:
        int accPhi = (int) (idx / (rDim));
        int accR = idx - (accPhi) * (rDim);
        float r = (accR - (rDim) * 0.5f) * discretizationStepsR;
        float phi = accPhi * discretizationStepsPhi;
        lines.add(new PVector(r, phi));
      }
    }

    drawLinesOnTop(edgeImg, lines);

    return lines;
  }

  void drawLinesOnTop(PImage edgeImg, ArrayList<PVector> lines) {
    for (int idx = 0; idx < lines.size(); ++idx) {
      PVector line = lines.get(idx);
      float r = line.x;
      float phi = line.y;

      // Cartesian equation of a line: y = ax + b
      // in polar, y = (-cos(phi)/sin(phi))x + (r/sin(phi))
      // => y = 0 : x = r / cos(phi)
      // => x = 0 : y = r / sin(phi)
      // compute the intersection of this line with the 4 borders of // the image

      int x0 = 0;
      int y0 = (int) (r / sin(phi));
      int x1 = (int) (r / cos(phi));
      int y1 = 0;
      int x2 = edgeImg.width;
      int y2 = (int) (-cos(phi) / sin(phi) * x2 + r / sin(phi));
      int y3 = edgeImg.width;
      int x3 = (int) (-(y3 - r / sin(phi)) * (sin(phi) / cos(phi)));

      // Finally, plot the lines

      stroke(204, 102, 0);
      if (y0 > 0) {
        if (x1 > 0)
          line(x0, y0, x1, y1);
        else if (y2 > 0)
          line(x0, y0, x2, y2);
        else
          line(x0, y0, x3, y3);
      } else {
        if (x1 > 0) {
          if (y2 > 0)
            line(x1, y1, x2, y2);
          else
            line(x1, y1, x3, y3);
        } else {
          line(x2, y2, x3, y3);
        }
      }
    }
  }
}