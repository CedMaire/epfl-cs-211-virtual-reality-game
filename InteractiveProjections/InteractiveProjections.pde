void settings() {
  size(1000, 1000, P3D);
}

void setup() {
  // Nothing
}

// Scale factor.
float scale = 1;

// Rotation angle.
float thetaX = 0;
float thetaY = 0;

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      thetaX += .1;
    } else if (keyCode == DOWN) {
      thetaX -= .1;
    } else if (keyCode == RIGHT) {
      thetaY += .1;
    } else if (keyCode == LEFT) {
      thetaY -= .1;
    }
  }
}

void mouseDragged() {
  if (mouseY - pmouseY < 0) {
    if (scale <= 5) {
      scale += .1;
    }
  } else {
    if (scale >= .2) {
      scale -= .1;
    }
  }
}

void draw() {
  background(255, 255, 255);

  My3DPoint eye = new My3DPoint(0, 0, -5000);
  My3DPoint origin = new My3DPoint(0, 0, 0);
  My3DBox input3DBox = new My3DBox(origin, 100 * scale, 150 * scale, 300 * scale);

  translate(width/2, height/3);

  // Rotated and translated
  float[][] transform2 = translationMatrix(0, 0, 0);
  input3DBox = transformBox(input3DBox, transform2);

  // Rotated, translated, and scaled
  float[][] transform3 = scaleMatrix(2, 2, 2);
  float[][] transformRotationX = rotateXMatrix(thetaX);
  float[][] transformRotationY = rotateYMatrix(thetaY);
  input3DBox = transformBox(transformBox(transformBox(input3DBox, transformRotationY), transformRotationX), transform3);
  projectBox(eye, input3DBox).render();
}

My3DBox transformBox(My3DBox box, float[][] transformMatrix) {
  My3DPoint[] result = new My3DPoint[box.p.length];

  for (int i = 0; i < result.length; ++i) {
    result[i] = euclidian3DPoint(matrixProduct(transformMatrix, homogeneous3DPoint(box.p[i])));
  }

  return new My3DBox(result);
}

My3DPoint euclidian3DPoint (float[] a) {
  My3DPoint result = new My3DPoint(a[0] / a[3], a[1] / a[3], a[2] / a[3]);

  return result;
}

float[] matrixProduct(float[][] a, float[] b) {
  float[] result =
    {0, 
    0, 
    0, 
    0};

  for (int i = 0; i < a.length; ++i) {
    for (int k = 0; k < b.length; ++k) {
      result[i] += a[i][k] * b[k];
    }
  }

  return result;
}

float[][] rotateXMatrix(float angle) {
  return (new float[][] {
    {1, 0, 0, 0}, 
    {0, cos(angle), -sin(angle), 0}, 
    {0, sin(angle), cos(angle), 0}, 
    {0, 0, 0, 1}});
}

float[][] rotateYMatrix(float angle) {
  return (new float[][] {
    {cos(angle), 0, sin(angle), 0}, 
    {0, 1, 0, 0}, 
    {-sin(angle), 0, cos(angle), 0}, 
    {0, 0, 0, 1}});
}

float[][] rotateZMatrix(float angle) {
  return (new float[][] {
    {cos(angle), -sin(angle), 0, 0}, 
    {sin(angle), cos(angle), 0, 0}, 
    {0, 0, 1, 0}, 
    {0, 0, 0, 1}});
}

float[][] scaleMatrix(float x, float y, float z) {
  return (new float[][] {
    {x, 0, 0, 0}, 
    {0, y, 0, 0}, 
    {0, 0, z, 0}, 
    {0, 0, 0, 1}});
}

float[][] translationMatrix(float x, float y, float z) {
  return (new float[][] {
    {1, 0, 0, x}, 
    {0, 1, 0, y}, 
    {0, 0, 1, z}, 
    {0, 0, 0, 1}});
}

float[] homogeneous3DPoint(My3DPoint point) {
  return (new float[] {point.x, point.y, point.z, 1});
}

My2DBox projectBox(My3DPoint eye, My3DBox box) {
  My2DPoint[] points = new My2DPoint[box.p.length];

  for (int i = 0; i < points.length; ++i) {
    points[i] = projectPoint(eye, box.p[i]);
  }

  return new My2DBox(points);
}

My2DPoint projectPoint(My3DPoint eye, My3DPoint point) {
  float[][] T =
    {{1, 0, 0, -eye.x}, 
    {0, 1, 0, -eye.y}, 
    {0, 0, 1, -eye.z}, 
    {0, 0, 0, 1}};
  float[][] P =
    {{1, 0, 0, 0}, 
    {0, 1, 0, 0}, 
    {0, 0, 1, 0}, 
    {0, 0, -1/eye.z, 0}};
  float[] pointMatrix =
    {point.x, 
    point.y, 
    point.z, 
    1};
  float[][] PT =
    {{0, 0, 0, 0}, 
    {0, 0, 0, 0}, 
    {0, 0, 0, 0}, 
    {0, 0, 0, 0}};
  float[] result =
    {0, 
    0, 
    0, 
    0};

  for (int row = 0; row < P.length; ++row) {
    for (int column = 0; column < T[row].length; ++column) {
      for (int k = 0; k < P[row].length; ++k) {
        PT[row][column] += P[row][k] * T[k][column];
      }
    }
  }

  for (int i = 0; i < PT.length; ++i) {
    for (int k = 0; k < pointMatrix.length; ++k) {
      result[i] += PT[i][k] * pointMatrix[k];
    }
  }

  for (int i = 0; i < result.length; ++i) {
    result[i] = result[i] / result[result.length - 1];
  }

  return new My2DPoint(result[0], result[1]);
}

class My2DPoint {
  float x, y;

  My2DPoint(float x, float y) {
    this.x = x;
    this.y = y;
  }
}

class My3DPoint {
  float x, y, z;

  My3DPoint(float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
}

class My2DBox {
  My2DPoint[] s;

  My2DBox(My2DPoint[] s) {
    this.s = s;
  }

  void render() {
    for (int i = 0; i < 4; ++i) {
      if (i != 3) {
        line(s[i].x, s[i].y, s[i + 1].x, s[i + 1].y);
      } else {
        line(s[i].x, s[i].y, s[0].x, s[0].y);
      }
    }
    for (int i = 4; i < 8; ++i) {
      if (i != 7) {
        line(s[i].x, s[i].y, s[i + 1].x, s[i + 1].y);
      } else {
        line(s[i].x, s[i].y, s[4].x, s[4].y);
      }
    }
    for (int i = 0; i < 4; ++i) {
      line(s[i].x, s[i].y, s[i + 4].x, s[i + 4].y);
    }
  }
}

class My3DBox {
  My3DPoint[] p;

  My3DBox(My3DPoint origin, float dimX, float dimY, float dimZ) {
    float x = origin.x;
    float y = origin.y;
    float z = origin.z;
    this.p = new My3DPoint[]{new My3DPoint(x, y+dimY, z+dimZ), 
      new My3DPoint(x, y, z+dimZ), 
      new My3DPoint(x+dimX, y, z+dimZ), 
      new My3DPoint(x+dimX, y+dimY, z+dimZ), 
      new My3DPoint(x, y+dimY, z), 
      origin, 
      new My3DPoint(x+dimX, y, z), 
      new My3DPoint(x+dimX, y+dimY, z)
    };
  }

  My3DBox(My3DPoint[] p) {
    this.p = p;
  }
}